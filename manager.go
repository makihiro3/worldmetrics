package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/binary"
	"encoding/gob"
	"errors"
	"net"
	"time"

	bolt "go.etcd.io/bbolt"
)

var IPv4Mask = net.CIDRMask(22, 32)
var IPv6Mask = net.CIDRMask(40, 128)
var ErrNotFound = errors.New("Not found")
var ErrDBInvalid = errors.New("DB Invalid")

type IDManager struct {
	db    *bolt.DB
	Block cipher.Block
}

func (m *IDManager) Close() error {
	return m.db.Close()
}

func NewManager(path string) (*IDManager, error) {
	db, err := bolt.Open(path, 0644, nil)
	if err != nil {
		return nil, err
	}
	var key []byte
	err = db.Update(func(tx *bolt.Tx) (err error) {
		_, err = tx.CreateBucketIfNotExists([]byte("id"))
		if err != nil {
			return
		}
		_, err = tx.CreateBucketIfNotExists([]byte("access"))
		if err != nil {
			return
		}
		b, err := tx.CreateBucketIfNotExists([]byte("key"))
		if err != nil {
			return
		}
		key = b.Get([]byte("key"))
		if key == nil {
			key = make([]byte, Size)
			_, err := rand.Read(key)
			if err != nil {
				return err
			}
			err = b.Put([]byte("key"), key)
			if err != nil {
				return err
			}
		}
		return
	})
	if err != nil {
		db.Close()
		return nil, err
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		db.Close()
		return nil, err
	}
	return &IDManager{db: db, Block: block}, nil
}

type idRecord struct {
	Created time.Time `json:"created"`
	Access  ID        `json:"access"`
	Admin   ID        `json:"admin"`
}

func (m *IDManager) NewID() (ID, error) {
	var id ID
	id[0] = 0
	id[1] = 0
	_, err := rand.Read(id[2:])
	var ret ID
	m.Block.Encrypt(ret[:], id[:])
	return ret, err
}

func (m *IDManager) New() (r idRecord, err error) {
	r.Admin, err = m.NewID()
	if err != nil {
		return
	}
	r.Access, err = m.NewID()
	if err != nil {
		return
	}
	r.Created = time.Now().UTC()
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(r)
	if err != nil {
		return
	}
	val := buf.Bytes()

	err = m.db.Update(func(tx *bolt.Tx) error {
		var err error
		b := tx.Bucket([]byte("id"))
		err = b.Put(r.Admin[:], val)
		if err != nil {
			return err
		}
		b = tx.Bucket([]byte("access"))
		_, err = b.CreateBucket(r.Access[:])
		return err
	})
	return
}

func (m *IDManager) IsValid(id ID) bool {
	var decrypted ID
	m.Block.Decrypt(decrypted[:], id[:])
	return (decrypted[0] | decrypted[1]) == 0
}

type accessRecord struct {
	Addr net.IP
	Area string
}

func (m *IDManager) Access(access ID, addr net.IP, area string) (err error) {
	if !m.IsValid(access) {
		return ErrNotFound
	}
	key := make([]byte, 8)
	binary.LittleEndian.PutUint64(key[:], uint64(time.Now().UnixNano()))
	var mask net.IPMask
	if addr.To4() == nil {
		mask = IPv6Mask
	} else {
		mask = IPv4Mask
	}
	addr = addr.Mask(mask)
	r := accessRecord{Addr: addr, Area: area}
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(r)
	if err != nil {
		return
	}
	val := buf.Bytes()

	return m.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("access")).Bucket(access[:])
		if b == nil {
			return ErrNotFound
		}
		return b.Put(key, val)
	})
}

type stat struct {
	Time time.Time `json:"time"`
	Addr net.IP    `json:"addr"`
	Area string    `json:"area"`
}

func (m *IDManager) Stats(admin ID) ([]stat, error) {
	if !m.IsValid(admin) {
		return nil, ErrNotFound
	}
	var stats []stat
	err := m.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("id")).Get(admin[:])
		if b == nil {
			return ErrNotFound
		}
		buf := bytes.NewBuffer(b)
		dec := gob.NewDecoder(buf)
		var record idRecord
		if err := dec.Decode(&record); err != nil {
			return err
		}
		a := tx.Bucket([]byte("access")).Bucket(record.Access[:])
		if a == nil {
			return ErrDBInvalid
		}

		return a.ForEach(func(k, v []byte) error {
			t := time.Unix(0, int64(binary.BigEndian.Uint64(k))).UTC()
			buf := bytes.NewBuffer(v)
			dec := gob.NewDecoder(buf)
			var record accessRecord
			if err := dec.Decode(&record); err != nil {
				return err
			}
			stats = append(stats, stat{Time: t, Addr: record.Addr, Area: record.Area})
			return nil
		})
	})
	return stats, err
}

func (m *IDManager) RemoveExpired(d time.Duration) error {
	expired := make([]byte, 8)
	binary.BigEndian.PutUint64(expired, uint64(time.Now().Add(-d).UTC().UnixNano()))
	return m.db.Update(func(tx *bolt.Tx) error {
		a := tx.Bucket([]byte("access")).Cursor()
		for k, _ := a.First(); k != nil; k, _ = a.Next() {
			b := a.Bucket().Cursor()
			k2, _ := b.Seek(expired)
			if k2 == nil {
				break
			}
			k2, _ = b.Prev()
			for ; k2 != nil; k2, _ = b.Prev() {
				if err := b.Delete(); err != nil {
					return err
				}
			}
		}
		return nil
	})
}
