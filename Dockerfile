FROM golang:1.14
WORKDIR /worldmetrics
COPY . .
RUN go get -d
RUN CGO_ENABLED=0 make

FROM busybox
COPY --from=0 /worldmetrics/worldmetrics ./
USER daemon
EXPOSE 8000
CMD ["./worldmetrics"]
