worldmetrics: assets.go handler.go id.go main.go manager.go
	go build

assets.go: test.png index.html tools/embed.go
	go run tools/embed.go index:index.html png:test.png >$@

clean:
	$(RM) worldmetrics assets.go

.PHONY: clean
