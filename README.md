# worldmetrics

World mertics（本サービス）はVRChatのWorld上にてVRC PanoramaによるHTTPアクセスを用いてWorldの利用状況を収集するサービスです。

VRC PanoramaはWorld外にあるインターネット上の画像データを表示する仕組みですが、外部から画像を取得する仕組みとして取得元にアクセスログが残るという原理を用いて動作しています。

## 取得するデーターの詳細

- Timestamp
- IP version(IPv4/IPv6)
- Source Network
  - IPv4の場合は/22でマスクしたnetwork address
  - IPv6の場合は/40でマスクしたnetwork address
- Cloudflareにて判定された国情報
  - https://support.cloudflare.com/hc/en-us/articles/200168236-Configuring-Cloudflare-IP-Geolocation

## データーの保持期限

- アクセスログを原則12ヶ月以上保持します
  - データー量によっては短くなることがあります
- アクセスログを13ヶ月以内に削除します
