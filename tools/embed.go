package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	fmt.Print("package main\n")
	for _, v := range os.Args[1:] {
		ts := strings.Split(v, ":")
		b, err := ioutil.ReadFile(ts[1])
		if err != nil {
			panic(err)
		}
		fmt.Printf("\nvar %s = []byte{", ts[0])
		for i := 0; i < len(b); i += 16 {
			fmt.Print("\n\t")
			j := i + 16
			if j >= len(b) {
				j = len(b)
			}
			var line []string
			for _, v := range b[i:j] {
				t := fmt.Sprintf("0x%02x,", v)
				line = append(line, t)
			}
			fmt.Print(strings.Join(line, " "))

		}
		fmt.Print("\n}\n")
	}
}
