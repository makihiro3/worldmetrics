package main

import (
	"encoding/base64"
	"errors"
)

const (
	Size = 16
)

var ErrInvalid = errors.New("Invalid format")

type ID [Size]byte

func Parse(s string) (ID, error) {
	var id ID
	rawId, err := base64.RawURLEncoding.DecodeString(s)
	if err != nil || len(rawId) != Size {
		return id, ErrInvalid
	}
	for i := range id {
		id[i] = rawId[i]
	}
	return id, nil
}

func (id ID) String() string {
	return base64.RawURLEncoding.EncodeToString(id[:])
}

func (id ID) MarshalText() ([]byte, error) {
	return []byte(id.String()), nil
}
