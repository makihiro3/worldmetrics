package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"golang.org/x/time/rate"
)

var (
	BatchInterval   = flag.Duration("bactch", time.Hour*24*7, "batch interval for expire")
	Expire          = flag.Duration("expire", time.Hour*24*366, "exipre access log")
	ShutdownTimeout = flag.Duration("shutdown", time.Second*5, "server graceful shutdown timeout")
	DBPath          = flag.String("dbpath", "db.bolt", "database filepath")
	Port            = flag.Uint("port", 8000, "listen port")
	PublicCC        = flag.String("cache-control", "public, max-age=3600", "public response cache-control")
	Every           = flag.Duration("every", time.Second, "assign token bucket add token every")
	Bucket          = flag.Int("bucket", 60, "assing token bucket size")
)

func sighandle(srv *http.Server, m *IDManager) {
	ticker := time.NewTicker(*BatchInterval)
	defer ticker.Stop()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	for {
		select {
		case <-ticker.C:
			m.RemoveExpired(*Expire)
		case <-c:
			ctx, cancel := context.WithTimeout(context.Background(), *ShutdownTimeout)
			defer cancel()
			if err := srv.Shutdown(ctx); err != nil {
				log.Print(err)
			}
			return
		}
	}
}

func main() {
	flag.VisitAll(func(f *flag.Flag) {
		if s := os.Getenv(strings.ToUpper(f.Name)); s != "" {
			f.Value.Set(s)
		}
	})
	flag.Parse()
	log.Println("open db:", *DBPath)
	m, err := NewManager(*DBPath)
	if err != nil {
		log.Fatal(err)
	}
	defer m.Close()
	h := handler{
		Data:     png,
		Index:    index,
		Manager:  m,
		Limiter:  rate.NewLimiter(rate.Every(*Every), *Bucket),
		PublicCC: *PublicCC,
	}
	m.RemoveExpired(*Expire)
	listen := fmt.Sprintf(":%d", *Port)
	log.Println("start server", listen)
	srv := http.Server{Addr: listen, Handler: &h}
	go sighandle(&srv, m)
	err = srv.ListenAndServe()
	log.Print(err)
	log.Print("finished")
}
