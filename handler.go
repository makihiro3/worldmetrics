package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strings"

	"golang.org/x/time/rate"
)

type handler struct {
	Data     []byte
	Index    []byte
	Manager  *IDManager
	Limiter  *rate.Limiter
	PublicCC string
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println(r.RemoteAddr, r.URL.Host, r.Method, r.URL.Path, r.UserAgent())
	io.Copy(ioutil.Discard, r.Body)
	r.Body.Close()
	if strings.HasPrefix(r.URL.Path, "/l/") {
		if r.Method != http.MethodGet {
			http.Error(w, "405 Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}
		h.image(w, r)
		return
	}
	if strings.HasPrefix(r.URL.Path, "/a/") {
		if r.Method != http.MethodGet {
			http.Error(w, "405 Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}
		h.stats(w, r)
		return
	}
	if r.URL.Path == "/assign" {
		if r.Method != http.MethodPost {
			http.Error(w, "405 Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}
		if !h.Limiter.Allow() {
			http.Error(w, "429 Too Many Requests", http.StatusTooManyRequests)
			return
		}
		h.assign(w, r)
		return
	}
	if r.URL.Path == "/" {
		w.Header().Set("Cache-Control", h.PublicCC)
		w.Write(h.Index)
		return
	}
	w.Header().Set("Cache-Control", h.PublicCC)
	http.NotFound(w, r)
}

func (h *handler) assign(w http.ResponseWriter, r *http.Request) {
	record, err := h.Manager.New()
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Cache-Control", "private, no-store")
	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	err = enc.Encode(record)
	if err != nil {
		log.Print(err)
	}
}

func (h *handler) image(w http.ResponseWriter, r *http.Request) {
	area := r.Header.Get("CF-IPCountry")
	addr := net.ParseIP(r.Header.Get("CF-Connecting-IP"))
	id := strings.TrimPrefix(r.URL.Path, "/l/")
	validId, err := Parse(id)
	if err != nil {
		w.Header().Set("Cache-Control", h.PublicCC)
		http.NotFound(w, r)
		return
	}
	err = h.Manager.Access(validId, addr, area)
	if err == ErrNotFound {
		w.Header().Set("Cache-Control", h.PublicCC)
		http.NotFound(w, r)
		return
	} else if err != nil {
		log.Print(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Cache-Control", "no-cache")
	w.Write(h.Data)
}

func (h *handler) stats(w http.ResponseWriter, r *http.Request) {
	id := strings.TrimPrefix(r.URL.Path, "/a/")
	validId, err := Parse(id)
	if err != nil {
		w.Header().Set("Cache-Control", h.PublicCC)
		http.NotFound(w, r)
		return
	}
	data, err := h.Manager.Stats(validId)
	if err == ErrNotFound {
		w.Header().Set("Cache-Control", h.PublicCC)
		http.NotFound(w, r)
		return
	}
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Cache-Control", "private, no-store")
	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	err = enc.Encode(data)
	if err != nil {
		log.Print(err)
	}
}
